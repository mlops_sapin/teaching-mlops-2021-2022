import bottle  # type: ignore
import spacy  # type: ignore
import logging
from json import dumps
import sys

logging.info("Loading model..")
nlp = spacy.load("./models")


@bottle.route("/api/intent")
def intent_inference():
    '''
    Return a json file containing the inferences for the given sentence
    '''
    sentence = bottle.request.query['sentence']
    return dumps(do_intent_inference(sentence))


def do_intent_inference(sentence: str) -> dict:
    '''
    Return a json object containing the inferences for the given sentence
    '''
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    bottle.response.content_type = "application/json"
    return inference.cats


@bottle.route("/api/intent-supported-languages")
def supported_languages():
    '''
    Return a json array containing the supported languages
    '''
    bottle.response.content_type = "application/json"
    return dumps(["fr-FR"])


@bottle.route("/health")
def health_check_endpoint():
    '''
    Return the status of the service
    '''
    return bottle.HTTPResponse(status=200)


if __name__ == "__main__":
    bottle.run(bottle.app(), host='0.0.0.0', port=sys.argv[1],
               debug=True, reloader=True)
