import main


def test_do_intent_inference():
    assert len(main.do_intent_inference("TEST")) == 8


def test_supported_languages():
    assert main.supported_languages() == '["fr-FR"]'


def test_health_check_endpoint():
    assert main.health_check_endpoint().status == "200 OK"
