from locust import HttpUser, task  # type: ignore


class HelloWorldUser(HttpUser):

    @task
    def hello_world(self):
        self.client.get("/api/intent?sentence=bonjour")
