https://tinyurl.com/mlops-paper

Groupe : Lucas Guédon, Alexandre Gissaud et Imad Boudroua

# Réponses aux questions

## "Vos services sont vachement gourmand coté ML. Tu peux me fournir la taille de ton image docker et la limite de RAM à définir sur les environnements stp ?"

Nous utilisons une image Docker provenant de DockerHub dont le lien est le suivant https://hub.docker.com/layers/python/library/python/3.8/images/sha256-90e0a08f49f46afa33dc1eed864576f8bea8eac9e33d58a76970211aed85c0d3?context=explore.
L'image fait une taille de 331.37 MB en taille compressée. Il faut ensuite rajouter les librairies python.
Pour la ram, un minimum 256 Mo a suffit pour le faire tourner l'application web en local.


## "Le modèle est viable jusqu’à quel trafic en production ? On souhaite avoir un P99 < 200ms"

| Name                               |  # reqs  |  # fails  |  Avg(ms)|Min(ms)|Max(ms)|Median(ms)|   req/s |failures/s |
|------------------------------------|----------|-----------|---------|-------|-------|----------|---------|-----------|
| GET /api/intent?sentence=bonjour   |   10567  | 0(0.00%)  |   142   |  105  |  3143 |     110  |  124.45 |   0.00    |

Response time percentiles (approximated) in ms
| Type   | Name                           |   50%|   66%|   75%|   80%|   90%|   95%|   98%|   99%| 99.9%|99.99%|  100%|# reqs|
|--------|--------------------------------|------|------|------|------|------|------|------|------|------|------|------|------|
| GET    | /api/intent?sentence=bonjour   |   110|   120|   120|   120|   140|   170|   450|  1100|  3100|  3100|  3100| 10567|

Nous n'arrivons pas à atteindre le p99 même avec le minimum de requête par seconde (un utilisateur soit environ 7 requêtes par secondes). Cependant, nous atteignons p95 < 200ms pour 124 requêtes par secondes.


## "On veut enrichir de la donnée dans la stack data avec ton modèle mais ça rame fort, t'as des idées pour améliorer les perfs ?"

On peut regrouper les requêtes utilisateurs dans des batchs puis les traiter toutes ensemble en une seule fois puis renvoyer les résultats à chaque utilisateurs.